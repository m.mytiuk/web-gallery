/* eslint-disable no-undef */
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const optionsImagesPath = {
  name: `[contenthash].[ext]`,
  publicPath(url) {
    if (process.env.NODE_ENV === 'production') {
      return `${process.env.BASE_URL}/${url}`;
    }
    return `/${url}`;
  },
};

module.exports = {
  chainWebpack: (config) => {
    const imagesRule = config.module.rule('images');
    const svgRule = config.module.rule('svg');
    const fileLoader = 'file-loader';

    config.module
      .rule('file')
      .test(/\.(woff2?|eot|ttf|otf)(\?.*)?$/)
      .use('url-loader')
      .loader('url-loader')
      .options({
        limit: 10000,
        name: 'assets/fonts/[name].[ext]',
      });

    imagesRule.uses.clear();
    svgRule.uses.clear();

    imagesRule
      .use(fileLoader)
      .loader(fileLoader)
      .options(optionsImagesPath);

    svgRule
      .use(fileLoader)
      .loader(fileLoader)
      .options(optionsImagesPath);
    svgRule.use('svg-transform-loader').loader('svg-transform-loader');

    config.module
      .rule('vue')
      .use('vue-svg-inline-loader')
      .loader('vue-svg-inline-loader')
      .options({});
  },
};
