import firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';

const firebaseConfig = {
  apiKey: 'AIzaSyCR0GetEistYjp_u8LfRRYtSHoGwUukPG4',
  authDomain: 'vue-practice-402bb.firebaseapp.com',
  databaseURL:
    'https://vue-practice-402bb-default-rtdb.europe-west1.firebasedatabase.app',
  projectId: 'vue-practice-402bb',
  storageBucket: 'vue-practice-402bb.appspot.com',
  messagingSenderId: '523894212349',
  appId: '1:523894212349:web:c4089956a1b6fef1a2e094',
  measurementId: 'G-JXCG5DKB82',
};

export const fireBaseApp = firebase;
export const db = firebase.initializeApp(firebaseConfig).database();
export const imagesRef = db.ref('images');
