import Vuex from 'vuex';
import Vue from 'vue';
import { fireBaseApp, imagesRef } from '../db';
import { vuexfireMutations, firebaseAction } from 'vuexfire';

const ADMIN_EMAIL = 'm.mytiuk@gmail.com';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    images: [],
    user: {},
  },

  mutations: {
    setUserData(state, user) {
      state.user = user;
    },
    resetUserData(state) {
      state.isUserAuth = {};
    },

    ...vuexfireMutations,
  },

  getters: {
    galleryCollection: (state) => {
      return state.images;
    },
    imageById: (state) => (id) => {
      return state.images[id];
    },

    getUser: ({ user }) => {
      return { name: user.name, email: user.email };
    },
    isUserAdmin: ({ user }) => {
      return user.email === ADMIN_EMAIL;
    },
    isUserAuth: ({ user }) => user?.email,
  },

  actions: {
    bindImages: firebaseAction(({ bindFirebaseRef }) => {
      return bindFirebaseRef('images', imagesRef);
    }),

    addImage(_, image) {
      const imageData = {
        src: image,
        comments: [],
        userWhoLikedImage: [],
      };
      imagesRef.push(imageData);
    },

    addComment(_, params) {
      const image = this.state.images[params.id];
      if (!image.comments) {
        image.comments = [];
      }
      image.comments.push({
        author: params.commentAuthor,
        text: params.commentText,
        date: Date.now(),
      });

      imagesRef.child(image['.key']).set(image);
    },

    toggleLikeImage(_, id) {
      const image = this.state.images[id];
      if (!image.userWhoLikedImage) {
        image.userWhoLikedImage = [];
      }

      const index = image.userWhoLikedImage.indexOf(this.state.user.email);
      if (index === -1) {
        image.userWhoLikedImage.push(this.state.user.email);
      } else {
        image.userWhoLikedImage.splice(index, 1);
      }

      imagesRef.child(image['.key']).set(image);
    },

    async googleSignIn({ commit }) {
      const provider = new fireBaseApp.auth.GoogleAuthProvider();
      try {
        const { user } = await fireBaseApp.auth().signInWithPopup(provider);
        commit('setUserData', user);
      } catch (error) {
        console.log(error);
      }
    },
  },
});
