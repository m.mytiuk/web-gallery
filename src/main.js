import Vue from 'vue'
import { rtdbPlugin } from 'vuefire'

import Gallery from './Gallery.vue'
import store from './store/index.js'

Vue.use(rtdbPlugin)

new Vue({
  el: '#app',
  render: h => h(Gallery),
  store
})
