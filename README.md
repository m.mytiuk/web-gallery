# web-gallery

#### [Звіт з практики](Практика%20Звіт%20Митюк.docx)
#### [Щоденник з практики](Щоденник%20практики%20Митюк%20Максим.doc)


## Project setup
```
npm run install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
